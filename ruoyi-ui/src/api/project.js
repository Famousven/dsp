import $http from '@/assets/js/http'

export function list(data) {
  return $http.post('project/project/index', data)
  // 为了测试，将原有逻辑变更为直接输出数据 TODO xuth 20221008
  return [
    {
      'privated': 1,
      'open_begin_time': 0,
      'code': '7e56d3dfe01241c498d45aa5c544fbb5',
      'owner_name': 'admin',
      'create_time': '2022-09-22 08:58:00',
      'member_code': '6v7be19pwman2fird04gqu53',
      'description': '阿斯顿',
      'archive': 0,
      'project_code': '7e56d3dfe01241c498d45aa5c544fbb5',
      'collected': 0,
      'organization_code': '6v7be19pwman2fird04gqu53',
      'template_code': 'd85f1bvwpml2nhxe94zu7tyi',
      'cover': 'http://easyproject.net/static/image/default/cover.png',
      'schedule': 0,
      'task_board_theme': 'simple',
      'deleted': 0,
      'auto_update_schedule': 0,
      'open_prefix': 0,
      'name': '其恶趣味',
      'open_task_private': 0,
      'access_control_type': 'open',
      'id': 136,
      'order': 0
    },
    {
      'privated': 1,
      'open_begin_time': 0,
      'code': 'ca2e289372764df49342ece19b97929b',
      'owner_name': 'admin',
      'create_time': '2022-09-22 08:47:24',
      'member_code': '6v7be19pwman2fird04gqu53',
      'description': '1111111',
      'archive': 0,
      'project_code': 'ca2e289372764df49342ece19b97929b',
      'collected': 0,
      'organization_code': '6v7be19pwman2fird04gqu53',
      'template_code': '0',
      'schedule': 0,
      'task_board_theme': 'simple',
      'deleted': 0,
      'auto_update_schedule': 0,
      'open_prefix': 0,
      'name': 'cash',
      'open_task_private': 0,
      'access_control_type': 'open',
      'id': 135,
      'order': 0
    },
    {
      'privated': 1,
      'open_begin_time': 0,
      'code': 'cf50726e99d746fa88107ed355d4a8cc',
      'owner_name': 'admin',
      'create_time': '2021-02-02 17:06:18',
      'member_code': '6v7be19pwman2fird04gqu53',
      'description': '111',
      'archive': 0,
      'project_code': 'cf50726e99d746fa88107ed355d4a8cc',
      'collected': 1,
      'organization_code': '6v7be19pwman2fird04gqu53',
      'template_code': '5b72d66e70b6404f93a74bbf5d4de2a4',
      'cover': 'http://localhost:8888/common/image?filePathName=/projectfile/project/cover/20210202/361aa715ecd14a38830e612dc40db567-22.png&realFileName=22.png',
      'schedule': 0,
      'task_board_theme': 'simple',
      'deleted': 0,
      'auto_update_schedule': 0,
      'open_prefix': 0,
      'name': '1111',
      'open_task_private': 0,
      'access_control_type': 'open',
      'id': 119,
      'order': 0
    },
    {
      'code': '0ecde82e3bed4789a8e201621ba79d9f',
      'member_code': '6v7be19pwman2fird04gqu53',
      'prefix': '',
      'description': 'test',
      'archive_time': '2022-09-21 11:01:52',
      'collected': 0,
      'organization_code': '6v7be19pwman2fird04gqu53',
      'cover': 'http://localhost:8888/common/image?filePathName=/projectfile/project/cover/20210202/7137ab1ea1354122bd70c361a7247a46-tomcatgo.jpg&realFileName=tomcatgo.jpg',
      'open_task_private': 0,
      'access_control_type': 'open',
      'id': 117,
      'order': 0,
      'privated': 1,
      'open_begin_time': 0,
      'owner_name': 'admin',
      'create_time': '2021-02-02 16:29:51',
      'archive': 1,
      'project_code': '0ecde82e3bed4789a8e201621ba79d9f',
      'template_code': '5b72d66e70b6404f93a74bbf5d4de2a4',
      'schedule': 0,
      'task_board_theme': 'simple',
      'deleted': 0,
      'auto_update_schedule': 0,
      'open_prefix': 0,
      'name': 'wuliang'
    },
    {
      'code': '3488bba47b8e48fc9cc75f5e5580cfb4',
      'member_code': '6v7be19pwman2fird04gqu53',
      'prefix': 'test',
      'description': 'lll\n',
      'archive_time': '',
      'collected': 0,
      'organization_code': '6v7be19pwman2fird04gqu53',
      'cover': 'http://122.112.164.217:8888/common/image?filePathName=/projectfile/project/cover/20200819/4d2aca9fb9fa4553a01ded90bd804778-22.png&realFileName=22.png',
      'open_task_private': 1,
      'access_control_type': 'open',
      'id': 103,
      'order': 0,
      'privated': 1,
      'open_begin_time': 1,
      'owner_name': 'admin',
      'create_time': '2020-07-06 17:07:26',
      'end_time': '2020-08-31',
      'begin_time': '2020-07-07',
      'archive': 0,
      'project_code': '3488bba47b8e48fc9cc75f5e5580cfb4',
      'template_code': '5b72d66e70b6404f93a74bbf5d4de2a4',
      'schedule': 28.57,
      'task_board_theme': 'simple',
      'deleted': 0,
      'auto_update_schedule': 1,
      'open_prefix': 1,
      'name': '水利部1号项目'
    },
    {
      'privated': 1,
      'open_begin_time': 0,
      'code': '8c4f887129e54068996e2d10a1c3bac9',
      'owner_name': 'admin',
      'create_time': '2020-07-06 10:22:43',
      'member_code': '6v7be19pwman2fird04gqu53',
      'prefix': '123',
      'description': 'lol\n',
      'archive': 0,
      'project_code': '8c4f887129e54068996e2d10a1c3bac9',
      'collected': 0,
      'organization_code': '6v7be19pwman2fird04gqu53',
      'template_code': '',
      'cover': 'http://122.112.164.217:8888/common/image?filePathName=/projectfile/project/cover/20200819/d0befdd0a131418d901c0e42d2ac9cfb-U字浅口V4.png&realFileName=U字浅口V4.png',
      'schedule': 33.33,
      'task_board_theme': 'simple',
      'deleted': 0,
      'auto_update_schedule': 0,
      'open_prefix': 1,
      'name': '水利部水资源调研项目',
      'open_task_private': 0,
      'access_control_type': 'open',
      'id': 95,
      'order': 0
    }
  ]
}

export function selfList(data) {
  return $http.post('project/project/selfList', data)
}

export function doData(data) {
  let url = 'project/project/save'
  if (data.projectCode) {
    url = 'project/project/edit'
  }
  return $http.post(url, data)
}

export function quit(code) {
  return $http.post('project/project/quit', { projectCode: code })
}
export function recycle(code) {
  return $http.post('project/project/recycle', { projectCode: code })
}
export function recovery(code) {
  return $http.post('project/project/recovery', { projectCode: code })
}
export function archive(code) {
  return $http.post('project/project/archive', { projectCode: code })
}
export function recoveryArchive(code) {
  return $http.post('project/project/recoveryArchive', { projectCode: code })
}
export function del(code) {
  return $http.post('project/project/delete', { projectCode: code })
}
export function read(code) {
  return $http.post('project/project/read', { projectCode: code })
}
export function analysis(data) {
  return $http.post('project/project/analysis', data)
}
export function _projectStats(data) {
  return $http.post('project/project/_projectStats', data)
}
export function _getProjectReport(data) {
  return $http.post('project/project/_getProjectReport', data)
}
export function getTopList(param) {
  return $http.post('project/project/getTopList', param)
}
export function getTask() {
  return $http.post('project/project/taskPriority')
}
/**
 * 前端发送post请求，等待后端处理。返回的应当是当前organization下所有
 * 设置了begin_time & end_time的tasks。
 * @returns
 */
// TODO 目前的问题是，现在能够读取当前组织下的所有项目的任务并且基于它们，实现
//      甘特图。但是我需要的是基于当前项目下的所有任务的甘特图。有着本质上的区别。
export function getTaskWithBeginEnd(code) {
  return $http.post('project/project/taskPriority', { projectCode: code })
}
