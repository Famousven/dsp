import Vue from 'vue'
import store from '@/store'
import Router from 'vue-router'
import Index from '@/views/index'
import Home from './home'
import { getStore, setStore } from '../assets/js/storage'
import { createRoute, isTokenExpired } from '../assets/js/utils'
import config from '../config/config'
import { refreshAccessToken } from '../api/common/common'

/**
 * 这是vuejs的router部分，将各个组件与主页关联在路由中，通过router目录下的index.js与home.js路由管理这些内容。
 */

/*
 * 首先，注册主页的地址，并传参给HOME_PAGE。
 */
let HOME_PAGE = config.HOME_PAGE
const currentOrganization = getStore('currentOrganization', true)
if (currentOrganization) {
  HOME_PAGE = HOME_PAGE + '/' + currentOrganization.code
  console.log('当前的主页链接地址为：', HOME_PAGE.toString())
}

Vue.use(Router) // 启用Vue-Router
/* /
 * 这里是要修改router的push方法，主要是为了避免在进行原地跳转的过程中，
 * 被报错promise uncaught
 */
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

/*
 * 创建数组routes放置可能的路由点。
 * 初始化时，仅放置主页节点。
 */
const routes = [].concat(
  Home
)
// const router = new Router({
//     routes: routers
// });
/*
 * 将menu进行层级分组。
 * 这里的数据来自windows.localStorage，而不是数据库，
 * 因此，menu在进行排序时，需要先判断是否存在。
 */
const menu = getStore('menu', true)
if (menu) {
  menu.forEach(function(v) {
    routes.push(createRoute(v))
    if (v.children) {
      v.children.forEach(function(v2) {
        routes.push(createRoute(v2))
        if (v2.children) {
          v2.children.forEach(function(v3) {
            routes.push(createRoute(v3))
          })
        }
      })
    }
  })
}

/**
 * router路由器的申明。
 */
const router = new Router({
  routes: [
    // 主页
    {
      path: '/',
      name: 'index',
      component: Index,
      children: routes
    },
    // {
    //     name: 'login',
    //     path: '/login',
    //     component: resolve => require(['@/views/login'], resolve),
    //     meta: {model: 'Login'},
    // },
    // 登录界面合集
    {
      name: 'member',
      path: '/member',
      component: resolve => require(['@/components/layout/UserLayout'], resolve),
      meta: { model: 'Login' },
      children: [
        {
          path: 'login',
          name: 'login',
          component: () => import(/* webpackChunkName: "user" */ '@/views/member/login'),
          meta: { model: 'Login' }
        },
        {
          path: 'register',
          name: 'register',
          component: () => import(/* webpackChunkName: "user" */ '@/views/member/Register'),
          meta: { model: 'Login' }
        },
        {
          path: 'forgot',
          name: 'forgot',
          component: () => import(/* webpackChunkName: "user" */ '@/views/member/forgot'),
          meta: { model: 'Login' }
        }
        // {
        //     path: 'register-result',
        //     name: 'registerResult',
        //     component: () => import(/* webpackChunkName: "user" */ '@/views/member/RegisterResult')
        // }
      ]
    },
    // {
    //     name: 'install',
    //     path: '/install',
    //     component: resolve => require(['@/views/error/install'], resolve),
    //     meta: {model: 'error'},
    // },
    // 通过email重置
    {
      name: 'resetEmail',
      path: '/reset/email',
      component: resolve => require(['@/views/reset/email'], resolve),
      meta: { model: 'error' }
    },
    // 404
    {
      name: '404',
      path: '/404',
      component: resolve => require(['@/views/error/404'], resolve),
      meta: { model: 'error' }
    },
    // 403
    {
      name: '403',
      path: '/403',
      component: resolve => require(['@/views/error/403'], resolve),
      meta: { model: 'error' }
    },
    // 500
    {
      name: '500',
      path: '/500',
      component: resolve => require(['@/views/error/500'], resolve),
      meta: { model: 'error' }
    }
  ]
})

/**
 * 在路由器即将工作，定位到新地址之前。需要执行的操作
 */
router.beforeEach((to, from, next) => {
  console.log('路由器将定位到新地址：', to)
  const tokenList = getStore('tokenList', true) // tokenList是一个将登陆账户信息保存在localStorage中的数据
  if (tokenList) {
    const refreshToken = tokenList.refreshToken
    const accessTokenExp = tokenList.accessTokenExp
    // 判断accessToken即将到期后刷新token
    if (accessTokenExp && isTokenExpired(accessTokenExp)) {
      refreshAccessToken(refreshToken).then(res => {
        // tokenList.accessToken = res.data.accessToken;
        // tokenList.accessTokenExp = res.data.accessTokenExp;
        // setStore('tokenList', tokenList);
        setStore('tokenList', res.data)
      })
    }
  }
  // 页面中转
  if (to.name === 'index' || to.path === '/index' || to.path === '/') {
    next({ path: HOME_PAGE })
    return false
  }
  // 无效页面跳转至首页
  if (!to.name && from.meta.model !== 'Login' && to.path !== HOME_PAGE) {
    next({ path: '/404' })
    return false
  }
  if (to.meta.model === 'Login' && store.state.logged) {
    next({ path: HOME_PAGE })
    return false
  }
  if (!store.state.logged && to.meta.model !== 'Login' && to.meta.model !== 'error') {
    next({
      name: 'login',
      query: { redirect: to.fullPath }
    })
    return false
  }
  next()
})

router.afterEach((to, from) => {
  // 预留 xuth 进行了注释
  console.log('路由器已经定位到新地址：', to)
  window.scrollTo(0, 0)
})

export default router
