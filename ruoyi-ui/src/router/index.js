import Vue from 'vue'
import store from '@/store'
import Router from 'vue-router'
import Index from '@/views/index'
import Home from './home'
import { getStore, setStore } from '../assets/js/storage'
import { createRoute, isTokenExpired } from '../assets/js/utils'
import config from '../config/config'
import { refreshAccessToken } from '../api/common/common'

/**
 * 这是vuejs的router部分，将各个组件与主页关联在路由中，通过router目录下的index.js与home.js路由管理这些内容。
 */

/*
 * 首先，注册主页的地址，并传参给HOME_PAGE。这是管理系统的homepage
 */
let HOME_PAGE_O = config.HOME_PAGE
const currentOrganization = getStore('currentOrganization', true)
if (currentOrganization) {
  HOME_PAGE_O = HOME_PAGE_O + '/' + currentOrganization.code
  console.log('当前的主页链接地址为：', HOME_PAGE_O.toString())
}

Vue.use(Router) // 启用Vue-Router

/* Layout */
import Layout from '@/layout'

/* /
 * 这里是要修改router的push方法，主要是为了避免在进行原地跳转的过程中，
 * 被报错promise uncaught
 */
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

/*
 * 创建数组routes放置可能的路由点。
 * 初始化时，仅放置主页节点。
 */
const routes = [].concat(
  Home
)
// const router = new Router({
//     routes: routers
// });
/*
 * 将menu进行层级分组。
 * 这里的数据来自windows.localStorage，而不是数据库，
 * 因此，menu在进行排序时，需要先判断是否存在。
 */
const menu = getStore('menu', true)
if (menu) {
  menu.forEach(function(v) {
    routes.push(createRoute(v))
    if (v.children) {
      v.children.forEach(function(v2) {
        routes.push(createRoute(v2))
        if (v2.children) {
          v2.children.forEach(function(v3) {
            routes.push(createRoute(v3))
          })
        }
      })
    }
  })
}

/**
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * roles: ['admin', 'common']       // 访问路由的角色权限
 * permissions: ['a:a:a', 'b:b:b']  // 访问路由的菜单权限
 * meta : {
    noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
    activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
  }
 */

// 以下的constantRouters与danymicRouters都是Router中router参数的一部分。
// 公共路由
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true
  },
  {
    path: '/register',
    component: () => import('@/views/register'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'index',
    children: [
      {
        path: 'index',
        component: () => import('@/views/index'),
        name: 'Index',
        meta: { title: '首页', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/definition',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'designer',
        component: () => import('@/views/workflow/definition/designer'),
        name: 'Designer',
        meta: { title: '流程设计', icon: '' }
      }
    ]
  },
  {
    path: '/work',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'start',
        component: () => import('@/views/workflow/work/start'),
        name: 'start',
        meta: { title: '发起流程', icon: '' }
      },
      {
        path: 'detail',
        component: () => import('@/views/workflow/work/detail'),
        name: 'Detail',
        meta: { title: '流程详情', icon: '' }
      }
    ]
  },
  {
    path: '/tool',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'build/index',
        component: () => import('@/views/tool/build/index'),
        name: 'FormBuild',
        meta: { title: '表单设计', icon: '' }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'profile',
        component: () => import('@/views/system/user/profile/index'),
        name: 'Profile',
        meta: { title: '个人中心', icon: 'user' }
      }
    ]
  },
  // xuth ： 20220927 添加project相关路由
  {
    path: '/project',
    component: Layout,
    meta: { title: '项目列表', icon: 'project' },
    children: [
      {
        name: 'projectIndex',
        path: 'index',
        component: () => import('@/views/home/index'),
        meta: { title: '项目', icon: 'project' }
      },
      {
        path: 'list/my',
        component: () => import('@/views/project/list/index'),
        name: 'MyProjects',
        meta: { title: '我的项目', icon: 'project' }
      },
      {
        path: 'analysis',
        component: () => import('@/views/project/analysis/index'),
        name: 'Analysis',
        meta: { title: '项目分析', icon: 'project' }
      },
      {
        path: 'list/collect',
        component: () => import('@/views/project/list/index'),
        name: 'collect',
        meta: { title: '我的收藏', icon: 'project' }
      },
      {
        path: 'archive',
        component: () => import('@/views/project/archive/index'),
        name: 'Archive',
        meta: { title: '已归档项目', icon: 'project' }
      },
      {
        path: 'recycle',
        component: () => import('@/views/project/recycle/index'),
        name: 'Recycle',
        meta: { title: '回收站', icon: 'project' }
      },
      {
        path: 'organization',
        component: () => import('@/views/organization/index'),
        name: 'organization',
        meta: { title: '我的组织', icon: 'project' }
      }
    ]
  },
  // xuth 20220928 添加project下tasks相关路由
  // TODO 还没有完成，在views文件夹下创建对应的视图文件，在component文件夹下创建对应的功能组件，在utils/asset/plugin等位置创建对应的逻辑
  {
    // 任务
    name: 'taskTab',
    path: '/project/space/task',
    hidden: true,
    component: Layout,
    children: [
      {
        // 任务看板
        name: 'task',
        path: ':code',
        component: () => import('@/views/project/space/task'),
        meta: { model: 122, info: { show_slider: false, is_inner: true }},
        children: [
          {
            name: 'taskDetail',
            path: 'detail/:taskCode',
            component: resolve => require(['@/views/project/space/taskdetail'], resolve)
          }
        ]
      },
      {
        // 任务详情
        name: 'taskdetail',
        path: 'detail/:taskCode',
        component: resolve => require(['@/views/project/space/taskdetail'], resolve),
        meta: { model: 'Project', info: { show_slider: false }}
      }
    ]
  },
  {
    // 文件
    name: 'filesTab',
    path: '/project/space/files',
    hidden: true,
    component: Layout,
    children: [
      {
        // 文件看板
        name: 'files',
        path: ':code',
        component: () => import('@/views/project/space/files'),
        meta: { model: 122, info: { show_slider: false, is_inner: true }}
      }
    ]
  },
  {
    // 概览
    name: 'overviewTab',
    path: '/project/space/overview',
    hidden: true,
    component: Layout,
    children: [
      {
        // 概览看板
        name: 'overview',
        path: ':code',
        component: () => import('@/views/project/space/overview'),
        meta: { model: 122, info: { show_slider: false, is_inner: true }}
      }
    ]
  },
  {
    // 版本
    name: 'featuresTab',
    path: '/project/space/features',
    hidden: true,
    component: Layout,
    children: [
      {
        // 版本看板
        name: 'features',
        path: ':code',
        component: () => import('@/views/project/space/features'),
        meta: { model: 122, info: { show_slider: false, is_inner: true }}
      }
    ]
  }
]

// 动态路由，基于用户权限动态去加载
export const dynamicRoutes = [
  {
    path: '/system/user-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:user:edit'],
    children: [
      {
        path: 'role/:userId(\\d+)',
        component: () => import('@/views/system/user/authRole'),
        name: 'AuthRole',
        meta: { title: '分配角色', activeMenu: '/system/user' }
      }
    ]
  },
  {
    path: '/system/role-auth',
    component: Layout,
    hidden: true,
    permissions: ['system:role:edit'],
    children: [
      {
        path: 'user/:roleId(\\d+)',
        component: () => import('@/views/system/role/authUser'),
        name: 'AuthUser',
        meta: { title: '分配用户', activeMenu: '/system/role' }
      }
    ]
  },
  {
    path: '/system/dict-data',
    component: Layout,
    hidden: true,
    permissions: ['system:dict:list'],
    children: [
      {
        path: 'index/:dictId(\\d+)',
        component: () => import('@/views/system/dict/data'),
        name: 'Data',
        meta: { title: '字典数据', activeMenu: '/system/dict' }
      }
    ]
  },
  {
    path: '/system/oss-config',
    component: Layout,
    hidden: true,
    permissions: ['system:oss:list'],
    children: [
      {
        path: 'index',
        component: () => import('@/views/system/oss/config'),
        name: 'OssConfig',
        meta: { title: '配置管理', activeMenu: '/system/oss' }
      }
    ]
  },
  {
    path: '/tool/gen-edit',
    component: Layout,
    hidden: true,
    permissions: ['tool:gen:edit'],
    children: [
      {
        path: 'index/:tableId(\\d+)',
        component: () => import('@/views/tool/gen/editTable'),
        name: 'GenEdit',
        meta: { title: '修改生成配置', activeMenu: '/tool/gen' }
      }
    ]
  }
]

console.log(constantRoutes)

// 防止连续点击多次路由报错
let routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(err => err)
}

export default new Router({
  base: process.env.VUE_APP_CONTEXT_PATH,
  mode: 'history', // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})
