/**
 * 这是来自管理系统的复制，作为备份先保存着
 */
import Vue from 'vue'
import Vuex from 'vuex'
// import state from './state'
import mutations from './mutations'
import actions from './actions'
import common from './modules/common'
import menu from './modules/menu'
// 这是从state.js中替换的代码。
import { getStore } from '@/assets/js/storage'

const userInfo = getStore('userInfo', true)
const theme = getStore('theme')
// import {getStore, setStore} from '@/assets/js/storage' 被 xuth 注释了，后续有需要可以释放
/**
 * 这里是对vuex容器进行了创建，通过store，可以直接使用对应js中的方法与参数。
 */

Vue.use(Vuex)

/**
 * 进行了store的申明，明显的是注册了modules，state，mutations以及actions。这些都可以从Vuex.Store class的
 * 源码中看到，对应vue的几个规则。在使用时，就可以通过对应的名称获取对应的值。
 * module 看作是对state的一种集合，每一个module都有属于自己的state，mutation等
 */
const store = new Vuex.Store({
  modules: {
    common,
    menu
  },
  state: {
    theme: theme || 'dark',
    logged: !!userInfo, // 登录状态
    userInfo: userInfo, // 用户信息
    organizationList: getStore('organizationList', true), // 能查看的组织列表
    currentOrganization: getStore('currentOrganization', true), // 当前组织
    system: getStore('system', true), // 系统配置
    windowLoading: false, // 窗口loading
    pageLoading: false, // 页面加载loading
    socketAction: '',
    boundClient: false // 是否绑定client

  },
  mutations,
  actions
})
export default store
