import Vue from 'vue'
import Vuex from 'vuex'
// import app from './modules/app'
import user from './modules/user'
import tagsView from './modules/tagsView'
import permission from './modules/permission'
import settings from './modules/settings'
import common from './modules/common'
import menu from './modules/menu'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'
import { getStore } from '@/assets/js/storage'

const userInfo = getStore('userInfo', true)
const theme = getStore('theme')

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    common,
    menu,
    app,
    user,
    tagsView,
    permission,
    settings
  },
  state: {
    themem: theme || 'dark',
    logged: !!userInfo, // 登录状态
    userInfo: userInfo, // 用户信息
    // organizationList: getStore('organizationList', true), // 能查看的组织列表
    // currentOrganization: getStore('currentOrganization', true), // 当前组织
    system: getStore('system', true), // 系统配置
    windowLoading: false, // 窗口loading
    pageLoading: false, // 页面加载loading
    socketAction: '',
    boundClient: false // 是否绑定client
  },
  mutations,
  actions,
  getters
})

export default store
