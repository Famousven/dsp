import { login, logout, getInfo } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    name: '',
    avatar: '',
    roles: [],
    permissions: [],
    memberCode: '', // 20221031t 添加了memberCode项到user中，user->store->main，就能够通过vue.$store.memberCode定位到
    member: [], // 20221031t 添加了member项到user中，user->store->main，就能够通过vue.$store.member定位到
    organizationList: [], // 20221031t 添加了organizationList项到user中，user->store->main，就能够通过vue.$store.organizationList定位到
    currentOrganization: '' // 20221031t 添加了currentOrganization项到user中，user->store->main，就能够通过vue.$store.currentOrganization定位到
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions
    },
    // 20221031 新增了SET_MEMBER与SET_MEMBER_CODE mutations，后续通过vue.$store.commit('函数名', argus)定位到
    SET_MEMBER: (state, member) => {
      state.member = member
    },
    SET_MEMBER_CODE: (state, memberCode) => {
      state.memberCode = memberCode
    },
    // 20221031 新增了SET_ORGANIZATION_LIST与SET_CURRENT_ORGANIZATION，后续通过vue.$store.commit('函数名', argus)定位到
    SET_ORGANIZATION_LIST: (state, organizationList) => {
      state.organizationList = organizationList
    },
    SET_CURRENT_ORGANIZATION: (state, currentOrganization) => {
      state.currentOrganization = currentOrganization
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      const username = userInfo.username.trim()
      const password = userInfo.password
      const code = userInfo.code
      const uuid = userInfo.uuid
      return new Promise((resolve, reject) => {
        login(username, password, code, uuid).then(res => {
          setToken(res.data.token)
          // login成功后，对应的token、member、memberCode都应保存
          commit('SET_TOKEN', res.data.token)
          commit('SET_MEMBER', res.data.member)
          commit('SET_MEMBER_CODE', res.data.member.code)
          commit('SET_ORGANIZATION_LIST', res.data.organizationList)
          if (res.data.organizationList) {
            commit('SET_CURRENT_ORGANIZATION', res.data.organizationList[0])
          }
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo().then(res => {
          const user = res.data.user
          const avatar = (user.avatar == '' || user.avatar == null) ? require('@/assets/images/profile.jpg') : user.avatar
          if (res.data.roles && res.data.roles.length > 0) { // 验证返回的roles是否是一个非空数组
            commit('SET_ROLES', res.data.roles)
            commit('SET_PERMISSIONS', res.data.permissions)
          } else {
            commit('SET_ROLES', ['ROLE_DEFAULT'])
          }
          commit('SET_NAME', user.userName)
          commit('SET_AVATAR', avatar)
          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 退出系统
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_MEMBER', [])
          commit('SET_MEMBER_CODE', '')
          commit('SET_ROLES', [])
          commit('SET_PERMISSIONS', [])
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
